function DecisionList (tabList, optionList, choiceList, variables) {

   function toHashMap(arr, idName){
      return arr.reduce(function(hash, obj){
         hash.set(obj[idName], obj)
         return hash
      }, new Map())
   }

   this.tabList = toHashMap(tabList, "id")

   this.optionList = toHashMap(optionList, "id")

   this.choiceList = new Map()

   for(var [key, value] of this.optionList){
      var choicesForKey = choiceList.filter(
         function(choice){
            return choice.optionId === key
         }
      )
      this.choiceList.set(key, toHashMap(choicesForKey, "id"))
   }

   this.variables = variables

   //Set up the decisionList with default values from all the options
   this.decisionList = new Map()
   for(var [key, value] of this.optionList){
      this.decisionList.set(key, value.defaultChoiceId)
   }
}

DecisionList.prototype = {
   constructor: DecisionList,

   getTabIds: function(){
    var tabIds = []
    for(var [key, value] of this.tabList){
       tabIds.push(key)
    }
    return tabIds
   },
   getOptionIds: function(){
      var optionIds = []
      for(var [key, value] of this.optionList){
         optionIds.push(key)
      }
      return optionIds
   },
   getOptionIdsInTab:function(tabId){
    var optionIds=[]
    for(var [key,value] of this.optionList){
       if(value.tabId === tabId){
          optionIds.push(key)
       }
    }
    return optionIds
   },
   getChoiceIdsInOption:function(optionId){
    var choiceIds=[]
    for(var [key,value] of this.choiceList.get(optionId)){
          choiceIds.push(key)
    }
    return choiceIds
   },
   getTabDataById: function(tabId){
    return this.tabList.get(tabId)
   },
   getOptionDataById: function(optionId){
    return this.optionList.get(optionId)
   },
   getChoiceDataById: function(optionId, choiceId){
      return this.choiceList.get(optionId).get(choiceId)
   },
   isDecided: function(optionId, decisionId){
      return (this.decisionList.get(optionId) === decisionId) ? true : false
   },
   setDecision: function(optionId, decisionId){
      this.decisionList.set(optionId, decisionId)
   },
   getDecision: function(optionId){
      return this.decisionList.get(optionId)
   },
   getAllDecisions: function(){
      return this.decisionList
   },
   getVariable(variableName){
      var variableValues = []
      for(var [optionId, choices] of this.choiceList){
         var decidedOption = choices.get(this.decisionList.get(optionId))
         if(decidedOption.hasOwnProperty(variableName)){
            variableValues.push(decidedOption[variableName])
         }
      }
      return this.variables[variableName](variableValues)
   }
}
