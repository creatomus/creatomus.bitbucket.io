var CD_MARGIN = 40
var CD_BARHEIGHT = 75
var CD_TICKHEIGHT = 20

var CD_GRANT ={
   notApplicable : -1,
   notValid : 0,
   kredex20 : 20,
   kredex40 : 40
}

var CD_HUES = ["#1f77b4",
            "#ff7f0e",
            "#2ca02c",
            "#d62728",
            "#9467bd",
            "#8c564b",
            "#e377c2",
            "#7f7f7f",
            "#bcbd22",
            "#17becf"]

var CD_CATEGORIES = [ "Projekt",
                  "Soojustamine",
                  "Tehnosüsteemid",
                  "Funktsionaalne"]

var CD_UTILITIES = [ "heating",
                     "solarPower",
                     "electric",
                     "other",
                     "loanPayment"
                     ]
var tooltipBox
function displayTooltip(name,value){
      tooltipBox.attr("opacity",1)
      tooltipBox.select(".label").text(name)
      tooltipBox.select(".price").text(value)
}

function hideTooltip(){
      tooltipBox.attr("opacity",0)
}

//Creates the start and end accumulative values for all the items
function toAccumulativeValues(values, startValue, optAccessor){
   var accessor = null
   if (optAccessor != null) { accessor = optAccessor}
   else { accessor = function(d){return d} }

   var accumulator = startValue
   var rangesOut = []
   values.forEach(function(d,i){
      var obj = {}
      obj.data = d
      obj.start = accumulator
      obj.end = accumulator += accessor(d,i)
      obj.index = i
      rangesOut.push(obj)
   })
   return rangesOut
}
//
// //Event handler for loading the csv file
// d3.select("#upload")
//    .on("change", function(){
//       var file = d3.event.target.files[0];
//          if (file) {
//             var reader = new FileReader();
//             reader.onloadend = function(evt) {
//                var csvDataUrl = evt.target.result;
//                // The following call results in an "Access denied" error in IE.
//                data = d3.csv(csvDataUrl,
//                               function(data){
//                                  data.forEach(function(d){
//                                     d.price = +d.price
//                                     d.kredex = +d.kredex
//                                  })
//                                  DrawCosts(data)
//                               }
//                )
//             };
//             reader.readAsDataURL(file);
//          }
//    })

var diagramHeight, diagramWidth

function SetUpDiagram(objId){
   var svg = d3.select(objId)
   diagramHeight = +svg.attr("height")-2*CD_MARGIN
   diagramWidth = +svg.attr("width")-2*CD_MARGIN

   var container = svg.append("g")
      .classed("cost-container", true)
      .attr("transform", "translate(" + CD_MARGIN + "," + CD_MARGIN + ")" )
      .attr("width", diagramWidth)
      .attr("height", diagramHeight)

   tooltipBox = svg.append("g")
      .classed("tooltip", true)
      .attr("opacity", 1)
      .attr("transform", "translate(" + (+svg.attr("width"))/2 + ",50)")

   tooltipBox.append("text")
      .classed("label", true)
      .attr("y",10)
   tooltipBox.append("text")
      .classed("price", true)
      .attr("y",30)

   container.append("g")
      .classed("cost-bars", true)
      .attr("transform", "translate(0, 140)" )

   var categoryAxis = container.append("g")
      .classed("category", true)
      .classed("axis", true)
      .attr("transform", "translate(0, 110)" )

   categoryAxis.append("line")
      .attr("x2", diagramWidth)
      .classed("baseline", true)

   // categoryAxis.append("line")
   //    .attr("y2", CD_TICKHEIGHT)
   //    .classed("tick first", true)

   categoryAxis.append("g")
      .classed("name", true)

   categoryAxis.append("g")
      .classed("tag", true)

   var grantAxis = container.append("g")
      .classed("grant", true)
      .classed("axis", true)
      .attr("transform", "translate(0, 245)" )

   grantAxis.append("line")
      .attr("x2", diagramWidth)
      .classed("baseline", true)

   // grantAxis.append("line")
   //    .attr("y2", -CD_TICKHEIGHT)
   //    .classed("tick first", true)

   grantAxis.append("g")
      .classed("name", true)

   grantAxis.append("g")
      .classed("tag", true)

   var utilitiesGraph = svg.append("g")
      .classed("utilities-container", true)
      .attr("transform", "translate(" + (CD_MARGIN + diagramWidth/2) + ",550)" )



   var containerFormer = utilitiesGraph.append("g")
      .classed("container-former", true)
      .attr("transform", "translate(-100,0)")

   var containerNew = utilitiesGraph.append("g")
      .classed("container-new", true)
      .attr("transform", "translate(100,0)")

   utilitiesGraph.append("line")
      .classed("baseline", true)
      .attr("x1", -200)
      .attr("x2", 200)

   containerFormer.append("text")
      .classed("name", true)
      .attr("y", 35)
      .text("KULUD PRAEGU")

   containerNew.append("text")
      .classed("name", true)
      .attr("y", 35)
      .text("KULUD PÄRAST")

   containerFormer.append("text")
      .classed("total-former", true)
      .classed("tag", true)
      .attr("dy",-5)
      .text("20")

   containerNew.append("text")
      .classed("total-new", true)
      .classed("tag", true)
      .attr("dy",-5)
      .text("20")

   utilitiesGraph.append("g")
      .classed("connection-lines", true)

}

function UpdateDiagram(containerId, data){


   var totalCost = data.derivatives.get("costTotal")
   //var grantType = data.derivatives.get("grantPercentage")/100

   //Container for the graph
   //var tooltipBox = d3.select(containerId+" .tooltip")
   var costBars = d3.select(containerId+" .cost-bars")
   var categoryAxis = d3.select(containerId+" .category")
   var grantAxis = d3.select(containerId+" .grant")

   //Set up color scale
   var color = d3.scaleOrdinal()
      .domain(CD_CATEGORIES)
      .range(CD_HUES)

   //Convert the
   var itemsCostAccumulative = toAccumulativeValues(data.state.decisions, 0, function(d, i){return data.state.options.get(i).choices.get(d).price})
   var xAxis = d3.scaleLinear()
               .domain([0, totalCost])
               .range([0, diagramWidth]);


   //Handle drawing the cost bars
   var costRects = costBars.selectAll(".cost-bar").data(itemsCostAccumulative)

   costBars.exit().remove()

   costRects
      .enter()
         .append("rect")
         .classed("cost-bar", true)
      .merge(costRects)
         .attr("id", function(d,i){return "cost-"+i})
         .attr("x", function(d,i){ return xAxis(d.start) })
         .attr("y", 0)
         .attr("height", CD_BARHEIGHT)
         .attr("width", function(d,i){ return xAxis(d.end-d.start)})
         .style("fill", function(d,i){ return color(data.state.options.get(d.index).type) })
         .on("mouseover", function(d,i) {
            displayTooltip(data.state.options.get(d.index).name, data.state.options.get(d.index).choices.get(d.data).price+"€")
            // tooltipBox.attr("opacity",1)
            // tooltipBox.select(".label").text()
            // tooltipBox.select(".price").text()
         })
         .on("mouseout", function(d) {
            hideTooltip()
            // tooltipBox.attr("opacity",0)
         })



   //Create an axis for the different categories
   categoryCosts = CD_CATEGORIES
   .map(
      function(cat){
         var categoryCost = 0
         data.state.decisions.forEach(function(decisionId, optionId){
            if(data.state.options.get(optionId).type === cat){
               categoryCost+=data.state.options.get(optionId).choices.get(decisionId).price
            }
         })
         return { type:cat, cost: categoryCost }
      }
   ).filter(function(e){
      return e.cost > 0
   })

   var categoryCostsAccumulative = toAccumulativeValues(categoryCosts, 0, function(d){return d.cost})

   var categoryTickStart = categoryAxis.selectAll(".tick.start").data(categoryCostsAccumulative)
   var categoryTickEnd = categoryAxis.selectAll(".tick.end").data(categoryCostsAccumulative)


   categoryTickStart.enter()
         .append("line")
         .classed("tick", true)
         .classed("start", true)

   categoryAxis.selectAll(".tick.start")
         .attr("x1", function(d,i){return xAxis(d.start)})
         .attr("x2", function(d,i){return xAxis(d.start)})
         .attr("y2", CD_TICKHEIGHT)

   categoryTickEnd.enter()
         .append("line")
         .classed("tick", true)
         .classed("end", true)

   categoryAxis.selectAll(".tick.end")
         .attr("x1", function(d,i){return xAxis(d.end)})
         .attr("x2", function(d,i){return xAxis(d.end)})
         .attr("y2", CD_TICKHEIGHT)

   categoryTickStart.exit().remove()
   categoryTickEnd.exit().remove()

   //Handle the labels for all categories
   var catLabels = categoryAxis.select(".name").selectAll("text").data(categoryCostsAccumulative)
   var catTags = categoryAxis.select(".tag").selectAll("text").data(categoryCostsAccumulative)

   catLabels.enter()
      .append("text")
      .attr("y", -30 )

   catTags.enter()
      .append("text")
      .attr("y",-10 )

   categoryAxis.select(".name").selectAll("text")
      .text(function(d){return d.data.type})
      .attr("x", function(d){return xAxis((d.start+d.end)/2) } )

   categoryAxis.select(".tag").selectAll("text")
      .text(function(d){return Math.round((d.data.cost/totalCost)*100)+"%"})
      .attr("x", function(d){return xAxis((d.start+d.end)/2) } )

   catTags.exit().remove()
   catLabels.exit().remove()


   var labels = new Map([
      ["selfFinance" , "Omaosalus"],
      ["loan" , "Pangalaen"],
      ["kredexGrant","KredEx"]
   ])

   var costValues = []
   data.funding.forEach(function(cost,type){
      if(cost>0){
         costValues.push({
            type:type,
            cost:cost
         })
      }
   })
   costValuesAccumulative = toAccumulativeValues(costValues, 0, function(d){return d.cost})

   var grantTicksStart = grantAxis.selectAll(".tick.start").data(costValuesAccumulative)
   var grantTicksEnd = grantAxis.selectAll(".tick.end").data(costValuesAccumulative)

   grantTicksStart.enter()
      .append("line")
      .classed("tick", true)
      .classed("start",true)

   grantTicksEnd.enter()
         .append("line")
         .classed("tick", true)
         .classed("end",true)

   grantAxis.selectAll(".tick.start")
         .attr("x1", function(d,i){return xAxis(d.start)})
         .attr("x2", function(d,i){return xAxis(d.start)})
         .attr("y2", -CD_TICKHEIGHT)

   grantAxis.selectAll(".tick.end")
         .attr("x1", function(d,i){return xAxis(d.end)})
         .attr("x2", function(d,i){return xAxis(d.end)})
         .attr("y2", -CD_TICKHEIGHT)

   grantTicksStart.exit().remove()
   grantTicksEnd.exit().remove()

   var grantG = grantAxis.selectAll("g.grant").data(costValuesAccumulative)

   grantG.exit().remove()

   grantG
      .enter()
         .append("g")
         .classed("grant",true)
         .each(function(){
            d3.select(this).append("text")
               .classed("name", true)
               .attr("y",40)
            d3.select(this).append("text")
               .classed("tag", true)
               .attr("y",25)
         })
      .merge(grantG)
         .attr("transform", function(d){return "translate("+ xAxis((d.start+d.end)/2) + ",0)" } )
         .each(function(d){
            d3.select(this).select(".name")
               .text( function(d){return labels.get(d.data.type)})

            d3.select(this).select(".tag")
            .text( function(d){return d.data.cost+"€"})
         })

   var utilityLabels = new Map([
      ["heating" , "Küttekulud"],
      ["other" , "Muud kulud"],
      ["solarPower" , "Lokaalne energiatootmine"],
      ["electric" , "Ühistu elektrikulud"],
      ["loanPayment","Laenumakse"]
   ])

   var formerCosts = new Map()
   data.formerUtilities.forEach(function(value, key){
      if(key != "energyProduction"){formerCosts.set(key, value)}
   })

   var newCosts = new Map()
   data.utilities.forEach(function(value, key){
      if(key != "energyProduction"){newCosts.set(key, value)}
   })
   var solarGain = -data.utilities.get("energyProduction")

   var formerCostsAccumulative = toAccumulativeValues(formerCosts, 0, function(d,i){return d})
   var newCostsAccumulative = toAccumulativeValues(newCosts, 0, function(d,i){return d})

   var utilitiesAxis = d3.scaleLinear()
               .domain([0, 30])
               .range([0, 150]);

   var utilityColorscale = d3.scaleOrdinal()
      .domain(CD_UTILITIES)
      .range(CD_HUES)

   var utilitiesContainer = d3.select(containerId+" .utilities-container")

   var containerFormer = d3.select(containerId+" .container-former")
   var containerNew = d3.select(containerId+" .container-new")

   //Former costs
   containerFormer.selectAll(".utility-bar.former")
      .data(formerCostsAccumulative)
      .enter()
         .append("rect")
         .classed("utility-bar",true)
         .classed("former",true)

   d3.selectAll(".utility-bar.former")
      .attr("width", 50)
      .attr("height",function(d){return utilitiesAxis(d.end-d.start)})
      .attr("x","-25")
      .attr("y",function(d){return -utilitiesAxis(d.end)})
      .attr("fill", function(d){return utilityColorscale(d.index)})
      .on("mouseover", function(d,i) {
         displayTooltip(utilityLabels.get(d.index),d.data.toFixed(1))
      })
      .on("mouseout", function(d){
         hideTooltip()
      })

   d3.selectAll(".utility-bar.new").exit().remove()

   //energyProduction
   var productionBar = containerNew.select(".utility-bar.production")
   if(solarGain > 0){
      if(productionBar.empty()){
         productionBar = containerNew.append("rect")
            .classed("utility-bar", true)
            .classed("production", true)
      }
      productionBar.attr("width", 50)
                     .attr("height", utilitiesAxis(solarGain))
                     .attr("x","25")
                     .attr("y","0")
                     .attr("fill", utilityColorscale("solarPower"))
                     .on("mouseover", function(d,i) {
                        displayTooltip(utilityLabels.get("solarPower"),solarGain.toFixed(1))
                     })
                     .on("mouseout", function(d){
                        hideTooltip()
                     })
   }else {
      productionBar.remove()
   }

   containerNew.selectAll(".utility-bar.new")
      .data(newCostsAccumulative)
      .enter()
         .append("rect")
         .classed("utility-bar",true)
         .classed("new",true)

   d3.selectAll(".utility-bar.new")
      .attr("width", 50)
      .attr("height",function(d){return utilitiesAxis(d.end-d.start)})
      .attr("x","-25")
      .attr("y",function(d){return utilitiesAxis(solarGain-d.end)})
      .attr("fill", function(d){return utilityColorscale(d.index)})
      .on("mouseover", function(d,i) {
         displayTooltip(utilityLabels.get(d.index),d.data.toFixed(1))
      })
      .on("mouseout", function(d){
         hideTooltip()
      })

   d3.selectAll(".utility-bar.new").exit().remove()

   //Update the total counters
   var totalFormer = data.formerTotal
   d3.select(".total-former")
      .attr("y", -utilitiesAxis(totalFormer))
      .text(totalFormer.toFixed(1)+" €/m2*a")

   var totalNew = data.derivatives.get("totalUtilities")
   d3.select(".total-new")
      .attr("y", -utilitiesAxis(totalNew))
      .text(totalNew.toFixed(1)+" €/m2*a")

   //Connection lines
   var lines = [
      [formerCostsAccumulative[0].start, formerCostsAccumulative[0].start ,newCostsAccumulative[0].start-solarGain,newCostsAccumulative[0].start-solarGain],
      [formerCostsAccumulative[0].end, formerCostsAccumulative[0].end ,newCostsAccumulative[0].end-solarGain,newCostsAccumulative[0].end-solarGain],
      [formerCostsAccumulative[1].end, formerCostsAccumulative[1].end ,newCostsAccumulative[1].end-solarGain,newCostsAccumulative[1].end-solarGain],
      [formerCostsAccumulative[2].end, formerCostsAccumulative[2].end ,newCostsAccumulative[2].end-solarGain,newCostsAccumulative[2].end-solarGain]
   ]
   var xPos = [-75,-25,25,75]

   var connectionLine = d3.line()
      .curve(d3.curveBasis)
      .x(function(d,i){
         return xPos[i]
      })
      .y(function(d,i){
         return -utilitiesAxis(d)
      })

   var linesContainer = d3.select(".connection-lines")

   linesContainer.selectAll(".line")
      .data(lines)
      .enter()
      .append("path")
      .classed("line",true)

   linesContainer.selectAll(".line")
      .attr("d",connectionLine)

   linesContainer.selectAll(".line")
      .exit().remove()
}
