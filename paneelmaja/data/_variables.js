   INPUT_VARIABLES = {
   "price" : function(prices){
      //console.log(prices)
      var total = prices.reduce(function(total, item){
         return total+item
      },0)
      //console.log(total)
      return total
   },
   "grantType" : function(grantValues){
      var grant = 40
      grantValues.forEach(function(val){
         if (val<grant){
            grant = val
         }
      })
      return grant
   },
   "solarPower" : function(solar){
      return solar.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "boundaries.wallUValue": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "boundaries.roofUValue": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "boundaries.cellarUValue": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "boundaries.windowUValue": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "tBridge.cornerPsi": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "tBridge.intWallPsi": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "tBridge.floorPsi": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "tBridge.balconyPsi": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "tBridge.roofPsi": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "tBridge.cellarPsi": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "tBridge.windowPsi": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "ventilation.flowRatePerSqm": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "ventilation.exchangeEfficiency": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   },
   "ventilation.sfp": function(value){
      return value.reduce(function(total, next){
         return total+next
      }, 0)
   }
}
