INPUT_TAB = [
  {
    "id": "soojustus",
    "index": 0,
    "name": "Soojustamine",
    "backgroundImage":"img/soojustamine_alus.png"
  },
  {
     "id": "tehno",
     "index": 1,
     "name": "Tehnosüsteemid",
     "backgroundImage":"img/tehno.png"
  },
  // {
  //   "id": "energia",
  //   "index": 2,
  //   "name": "Lokaalne energiatootmine"
  // },
  {
    "id": "trepp",
    "index": 3,
    "name": "Trepikojad",
    //"backgroundImage":"img/trepp-01.png"
  },
  {
    "id": "fassaad",
    "index": 4,
    "name": "Fasaadid",
    "backgroundImage": "img/fassaad-00.png"
  }
]
