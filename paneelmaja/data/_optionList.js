INPUT_OPTION = [
  {
    "id": "so-fas",
    "name": "Fassaadid",
    "tabId": "soojustus",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Soojustamine"
  },
  {
    "id": "so-sos",
    "name": "Sokkel",
    "tabId": "soojustus",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Soojustamine"
  },
  {
    "id": "so-kas",
    "name": "Katus",
    "tabId": "soojustus",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Soojustamine"
  },
  {
    "id": "so-akn",
    "name": "Aknad",
    "tabId": "soojustus",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Soojustamine"
  },
  {
    "id": "te-ven",
    "name": "Ventilatsioonisüsteem",
    "tabId": "tehno",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Tehnosüsteemid"
  },
  {
    "id": "te-kys",
    "name": "Küttesüsteem",
    "tabId": "tehno",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Tehnosüsteemid"
  },
  {
    "id": "te-vek",
    "name": "Vee- ja kanalisatsioonisüsteem",
    "tabId": "tehno",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Tehnosüsteemid"
  },
  {
    "id": "en-ppa",
    "name": "Päikesepaneelid",
    "tabId": "tehno",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Tehnosüsteemid"
  },
  {
    "id": "tr-tam",
    "name": "Trepikoja eeskoda",
    "tabId": "trepp",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Funktsionaalne"
  },
  {
    "id": "tr-int",
    "name": "Trepikoja interjöör",
    "tabId": "trepp",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Funktsionaalne"
  },
  {
    "id": "fa-fas",
    "name": "Fassaadi kujundus",
    "tabId": "fassaad",
    "buttonType": "radio",
    "defaultChoiceId": "krohv",
    "type":"Funktsionaalne"
  },
  {
    "id": "fa-rod",
    "name": "Rõdude renoveerimine",
    "tabId": "fassaad",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Funktsionaalne"
  },
  {
    "id": "fa-ter",
    "name": "Terrasside ehitamine 1. korrusele",
    "tabId": "fassaad",
    "buttonType": "radio",
    "defaultChoiceId": "nullvalik",
    "type":"Funktsionaalne"
  }
]
